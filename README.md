#Install Couchbase server
```bash
$ wget https://packages.couchbase.com/releases/5.0.1/couchbase-server-community_5.0.1-ubuntu16.04_amd64.deb

$ sudo apt install gdebi-core
$ sudo gdebi couchbase-server-community_5.0.1-ubuntu16.04_amd64.deb

$ sudo ufw allow from any to any port 369,8091:8094,9100:9105,9998,9999,11209:11211,11214,11215,18091:18093,21100:21299 proto tcp
```

#Install PHP extension
```bash
# Only needed during first-time setup:
$ wget http://packages.couchbase.com/releases/couchbase-release/couchbase-release-1.0-4-amd64.deb

$ sudo dpkg -i couchbase-release-1.0-4-amd64.deb

# Will install or upgrade packages
$ sudo apt-get update
$ sudo apt-get install libcouchbase-dev build-essential php-dev zlib1g-dev
$ sudo pecl install couchbase

#Add mod to PHP
$ sudo cp ./couchbase.ini /etc/php/<version php>/mods-available/couchbase.ini

#Enable mod
$sudo phpenmod -v <version php> -s ALL couchbase

#Reload PHP fpm.
sudo php<version>-fpm reload
```

# Install project:
```
composer install

cp .env.example .env

php artisam key:generate

php artisam bucket-create:users
```
