<?php

namespace App\Console\Commands;

use CouchbaseCluster;
use Illuminate\Console\Command;

class CreateUsersBucket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bucket-create:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command create users bucket in couchbase.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $cluster = new CouchbaseCluster(config('database.connections.couchbase.host'));

        $cluster->authenticateAs(
            config('database.connections.couchbase.user'),
            config('database.connections.couchbase.password')
        );

        $cluster
            ->manager()
            ->createBucket('users', [
                'bucketType' => 'couchbase',
                'saslPassword' => '',
                'flushEnabled' => true
            ]);

        sleep(10);

        $bucketManager = $cluster->openBucket('users')->manager();

        $bucketManager->createN1qlPrimaryIndex();

        $bucketManager->createN1qlIndex('users_secondary_index', [
            'id',
            'name',
            'email',
        ]);
    }
}
