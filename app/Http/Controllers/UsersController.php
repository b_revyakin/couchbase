<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class UsersController extends Controller
{
    public function index()
    {
        return DB::table('users')->get();
    }

    public function store(Request $request)
    {
        $guid = (string) Uuid::generate();

        $data = [
            'id' => $guid,
            'name' => $request->name,
            'email' => $request->email,
        ];

        DB::table('users')
            ->key($guid)
            ->insert($data);

        return $data;
    }

    public function update(Request $request, $id)
    {
        DB::table('users')
            ->key($id)
            ->where('id', $id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
            ]);
    }

    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
    }
}
