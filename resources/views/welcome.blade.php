<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Couchbase</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <style>
    .full-height {
      height: 100vh;
    }

    .title {
      text-align: center;
      font-size: 84px;
    }
  </style>

  <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="app">
  <div class="full-height">
    <div class="content">
      <div class="title m-b-md">
        Couchbase
      </div>

      <users-index></users-index>
    </div>
  </div>
</div>
</body>

<script src="{{ mix('js/app.js') }}"></script>
</html>
